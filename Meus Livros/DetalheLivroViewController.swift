//
//  DetalheLivroViewController.swift
//  Meus Livros
//
//  Created by COTEMIG on 10/09/22.
//

import UIKit

class DetalheLivroViewController: UIViewController {
   
    var nomeLivro: String = ""
    var nomeAutor: String = ""
    var nomeEditora: String = ""
    var nomeImagem: String = ""
    
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var autor: UILabel!
    @IBOutlet weak var editora: UILabel!
    @IBOutlet weak var capa: UIImageView!
    
   
    override func viewDidLoad(){
        super.viewDidLoad()
        
        self.nome.text = self.nomeLivro
        self.autor.text = self.nomeAutor
        self.editora.text = self.nomeEditora
        self.capa.image = UIImage(named: self .nomeImagem)
    }
}
