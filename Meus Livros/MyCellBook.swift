//
//  MyCellBook.swift
//  Meus Livros
//
//  Created by COTEMIG on 10/09/22.
//

import UIKit

class MyCellBook: UITableViewCell {

    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var autor: UILabel!
    @IBOutlet weak var editora: UILabel!
    @IBOutlet weak var capa: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
