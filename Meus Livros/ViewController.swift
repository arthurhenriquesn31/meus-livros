//
//  ViewController.swift
//  Meus Livros
//
//  Created by COTEMIG on 10/09/22.
//

import UIKit
 
struct Livro {
    let nome: String
    let autor :String
    let editora:String
    let nomeImagemPequena:String
    let nomeImagemGrande:String
    
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var listaDeLivros: [Livro] = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeLivros.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelulaLivro",for: indexPath) as! MyCellBook
        let livro = self.listaDeLivros[indexPath.row]
        
        cell.nome.text = livro.nome
        cell.autor.text = livro.autor
        cell.editora.text = livro.editora
        cell.capa.image = UIImage(named: livro.nomeImagemPequena)
        cell.capa.image = UIImage(named: livro.nomeImagemGrande)
        
        return cell
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath: IndexPath){
        self.performSegue(withIdentifier: "abrirDetalhesLivro", sender: indexPath.row)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalhesViewController = segue.destination as!DetalheLivroViewController
        let indice = sender as! Int
        let livro = self.listaDeLivros[indice]
        detalhesViewController.nomeLivro = livro.nome
        detalhesViewController.nomeAutor = livro.autor
        detalhesViewController.nomeEditora = livro.editora
        detalhesViewController.nomeImagem = livro.nomeImagemGrande
    }
    
    @IBOutlet weak var TableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.TableView.dataSource = self
        
        self.TableView.delegate = self
       
        self.listaDeLivros.append(Livro(nome: "Guia dos Mochileiros das Galaxias", autor:"Douglas Adams", editora:"Editor Arqueiro", nomeImagemPequena:"capa_guia_pequeno" , nomeImagemGrande:"capa_guia_grande"))
        
        self.listaDeLivros.append(Livro(nome: "O menino Maluquinho", autor:"Ziraldo", editora:"Melhores Momentos", nomeImagemPequena:"capa_menino_pequno" , nomeImagemGrande:"capa_menino_grande"))
        
        self.listaDeLivros.append(Livro(nome: "1984", autor:"George Orwell", editora:"Editora Commpanhia das Letras", nomeImagemPequena:"capa_1984_pequno" , nomeImagemGrande:"capa_1984_grande"))
        TableView.dataSource = self
    }


}

